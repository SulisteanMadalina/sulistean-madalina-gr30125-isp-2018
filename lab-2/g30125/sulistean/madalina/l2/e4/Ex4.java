package g30125.sulistean.madalina.l2.e4;

import java.util.Scanner;

public class Ex4 {
public static void main(String[] args)
{
	Scanner in = new Scanner(System.in);
	System.out.println("Numarul de elemente din sir n= ");
	 int n = in.nextInt();
	int a[]=new int[n];
	int i;
    for(i=0;i<n;i++)
    {
    System.out.println("a[ "+i+" ]=");
     a[i]=in.nextInt();
    }
    int max=a[0];
    for(i=0;i<n;i++)
    if(max<a[i]) max=a[i];
    System.out.println("Valoarea maxima este "+max);
    in.close();
}
}
