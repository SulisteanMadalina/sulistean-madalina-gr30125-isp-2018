package g30125.sulistean.madalina.l2.e3;

import java.util.Scanner;

public class Ex3 {
	static int prim(int n)
	{
		int d=2;
		while(d*d<=n)
		{
			if(n%d==0)
				return 0;//nu este prim
			
			d++;
		}
		
		
		return 1;
	}
	public static void main (String[] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.println("A= ");
	    int a = in.nextInt();
	    System.out.println("B= ");
	    int b = in.nextInt();
	    for(int i=a;i<b;i++)
	    {
	    	if(prim(i)==1) 
	    	System.out.println(" "+i);
	    }
	
	    	in.close();
	}
}
