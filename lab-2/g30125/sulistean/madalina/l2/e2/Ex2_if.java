package g30125.sulistean.madalina.l2.e2;

import java.util.Scanner;

public class Ex2_if {
	public static void main (String[] args)
	{Scanner in = new Scanner(System.in);
	System.out.println("a= ");
    int a = in.nextInt();
    if(a==1) System.out.println("UNU");
    else if(a==2) System.out.println("DOI");
    else if(a==3) System.out.println("TREI");
    else if(a==4) System.out.println("PATRU");
    else if(a==5) System.out.println("CINCI");
    else if(a==6) System.out.println("SASE");
    else if(a==7) System.out.println("SAPTE");
    else if(a==8) System.out.println("OPT");
    else if(a==9) System.out.println("NOUA");
    else  System.out.println("ALTUL");
}
	
}
