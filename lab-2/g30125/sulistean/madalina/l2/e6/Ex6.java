package g30125.sulistean.madalina.l2.e6;
import java.util.Scanner;
public class Ex6 {
    static int factorial_recursiv(int n)
    {
        if (n == 0)
          return 1;
         
        return n*factorial_recursiv(n-1);
    }
    static int factorial_nerecursiv(int n)
    { int i,f=1;
    for(i=1;i<=n;i++)
    	f*=i;
    return f;
    }
    public static void main(String[] args)
    {
    	Scanner in=new Scanner(System.in);
    	System.out.println("n= ");
    	int n=in.nextInt();
    	int a,b;
    	a=factorial_recursiv(n);
    	System.out.println("Rezultatul factorialului recursiv este:"+a);
    	b=factorial_nerecursiv(n);
    	System.out.println("Rezultatul factorialului nerecursiv este:"+b);
    	in.close();
    	
    	
    }
}
