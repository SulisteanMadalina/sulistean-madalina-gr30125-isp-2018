package g30125.sulistean.madalina.l4.e4;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestAuthor {
	Author a = new Author("Mihai","mihai@email.com",'M');

	@Test
	public void testEmail()
	{
		assertEquals(a.toString(),"Mihai (M) at mihai@email.com");
	}
}
