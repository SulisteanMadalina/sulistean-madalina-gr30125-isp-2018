package g30125.sulistean.madalina.l4.e5;
import static org.junit.Assert.*;
import org.junit.Test;
import g30125.sulistean.madalina.l4.e4.*;

public class TestBook {

	@Test
	public void test()
	{
		Author a = new Author("author1","author1@email.com",'M');
		Book b = new Book("book1",a,102.42);
		
		assertEquals(b.toString(),"book1 by author-author1 (M) at author1@email.com");
				
	}
	
}