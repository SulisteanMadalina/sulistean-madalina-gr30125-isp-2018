package g30125.sulistean.madalina.l4.e3;

public class Circle {
	private double radius;
	private String color;
	
	public Circle()
	{
		this.radius=1.0;
		this.color="red";
	}
	
	public Circle(double r)
	{
		this.radius=r;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea()
	{
		return Math.PI*getRadius()*getRadius();
	}
}
