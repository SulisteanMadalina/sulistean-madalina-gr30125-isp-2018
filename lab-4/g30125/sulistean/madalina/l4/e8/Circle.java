package g30125.sulistean.madalina.l4.e8;

public class Circle extends Shape {
	private double radius;
	
	public Circle()
	{
		super();
		this.radius = 1.0;
	}
	
	public Circle(double r)
	{
		super();
		this.radius = r;
	}
	
	public Circle(double r, String c, boolean f)
	{
		super(c,f);
		this.radius = r;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public void setRadius(double r)
	{
		this.radius = r;
	}
	
	public double getArea()
	{
		return Math.PI*getRadius()*getRadius();
	}
	
	public double getPerimeter()
	{
		return 2*Math.PI*this.radius;
	}
	
	public String toString()
	{
		return "A Circle with radius="+this.radius+", which is a subclass of "+super.toString();
	}
}