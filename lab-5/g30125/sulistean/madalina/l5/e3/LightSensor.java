package g30125.sulistean.madalina.l5.e3;

import java.util.Random;
public class LightSensor extends Sensor{
	Random r=new Random();
	int val=r.nextInt(100);
	@Override
	public int readValue() {
		return this.val;
	}
}
