package g30125.sulistean.madalina.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.BLUE, 40,30,40,"A3",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,100,50,"1",true);
        b1.addShape(s2);
        Shape s3= new Rectangle(Color.RED,120,80,200,"3",false);
        b1.addShape(s3);
        
        b1.delete("1");
        
      
    }
}
